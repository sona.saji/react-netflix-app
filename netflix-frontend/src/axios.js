import axios from "axios";
import { baseUrl } from "./constants/constants";
// import React,{baseUrl} from './constants/constants'
// baseUrl

  const instance = axios.create({
    baseURL: baseUrl,
  });
export default instance
