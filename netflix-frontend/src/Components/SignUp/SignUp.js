import React,{useState} from 'react'
import axios from 'axios'

const SignUp = () => {
    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")
    const [password_confirmation,setPassword_confirmation] = useState("")
    function Register(){
       
        axios.post('http://localhost:3000/users',
        {
          user: {
            email : email,
            password :password,
            password_confirmation :password_confirmation,
          }
        }
        ).then(result => {
          console.log(result)
          console.log("token:" ,result.headers.authorization)
          window.alert("Email : "+ result.data.user.email)            
        })
        .catch(error => {
            window.alert(error.response.data.error)
            console.log(error)
        })

    }

  return (
    <section className="background-radial-gradient overflow-hidden">
      <div className="container px-4 py-5 px-md-5 text-center text-lg-start my-5">
        <div className="row gx-lg-5 align-items-center mb-5">
          <div className="col-lg-6 mb-5 mb-lg-0 login-col">
            <h1 className="my-5 display-5 fw-bold ls-tight login-head">
              The best offer <br />
              <span className="offer-data">for your business</span>
            </h1>
            <p className="mb-4 opacity-70 offer-desc">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit.
              Temporibus, expedita iusto veniam atque, magni tempora mollitia
              dolorum consequatur nulla, neque debitis eos reprehenderit quasi
              ab ipsum nisi dolorem modi. Quos?
            </p>
          </div>

          <div className="col-lg-6 mb-5 mb-lg-0 position-relative">
            <div id="radius-shape-1.1" className="position-absolute rounded-circle shadow-5-strong"></div>
            <div id="radius-shape-2.1" className="position-absolute shadow-5-strong"></div>

            <div className="card bg-glass">
              <div className="card-body px-4 py-5 px-md-5">
                {/* <!-- 2 column grid layout with text inputs for the first and last names --> */}
                {/* <div className="row">
                  <div className="col-md-6 mb-4">
                    <div className="form-outline">
                      <input type="text" id="form3Example1" className="form-control" >
                      <label className="form-label" for="form3Example1">First name</label>
                    </div>
                  </div>
                  <div className="col-md-6 mb-4">
                    <div className="form-outline">
                      <input type="text" id="form3Example2" className="form-control" />
                      <label className="form-label" for="form3Example2">Last name</label>
                    </div>
                  </div>
                </div> */}

                {/* <!-- Email input --> */}
                <div className="form-outline mb-4">
                <label className="form-label" htmlFor="form3Example3">Email</label>
                  <input type="email" id="form3Example3.1" placeholder="Eg:abc@email.com" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>

                {/* <!-- Password input --> */}
                <div className="form-outline mb-4">
                  <label className="form-label" htmlFor="form3Example4">Password</label>
                  <input type="password" id="form3Example4.1" placeholder="Password must contain 6 characters" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} />
                </div>

                {/* <!-- Password input --> */}
                <div className="form-outline mb-4">
                  <label className="form-label" htmlFor="form3Example4">Confirm Password</label>
                  <input type="password" id="form3Example4.2" placeholder="Password must contain 6 characters" className="form-control" value={password_confirmation} onChange={(e) => setPassword_confirmation(e.target.value)} />
                </div>

                {/* <!-- Submit button --> */}
                <div className='submit_button text-center'>
                  <button type="submit" className="w-50 btn btn-primary btn-block mb-4" onClick={Register}>
                    Sign up
                  </button>
                </div>

                {/* <!-- Register buttons --> */}
                <div className="text-center">
                  <p>or sign up with:</p>
                  <button type="button" className="btn btn-link btn-floating mx-1">
                    <i className="fab fa-facebook-f"></i>
                  </button>

                  <button type="button" className="btn btn-link btn-floating mx-1">
                    <i className="fab fa-google"></i>
                  </button>

                  <button type="button" className="btn btn-link btn-floating mx-1">
                    <i className="fab fa-twitter"></i>
                  </button>

                  <button type="button" className="btn btn-link btn-floating mx-1">
                    <i className="fab fa-github"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default SignUp