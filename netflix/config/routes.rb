Rails.application.routes.draw do
  devise_for :users,
             controllers: {
                 sessions: 'users/sessions',
                 registrations: 'users/registrations'
             }, defaults: { format: :json }
  get '/movie-data', to: 'movies#show'
end
